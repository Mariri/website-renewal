/**
* Add splide functionality
*/

const splide = () => {
  const anchorLink = document.querySelectorAll("a[href^='#']")

  //@ts-ignore
  const splide = new Splide('.js-main-splide', {
    direction: 'ttb',
    height: '100vh',
    wheel: true,
    arrows: false,
    pagination: false
  })

  splide.mount()

  //@ts-ignore
  const splideSub = new Splide('.js-sub-splide', {
    height: '292px',
    pagination: false,
    type: 'loop',
    drag: false
  })

  splideSub.mount()

  anchorLink.forEach(item => {
    item.addEventListener('click', (e) => {
      e.preventDefault()

      let sliderIndex = Number(String(item.getAttribute('href')).replace('#', ''))
      splide.go(sliderIndex)
    });
  })
}

export default splide