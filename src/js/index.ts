import forEachPolyfill from './components/forEachPolyfill';
import splide from './components/splide';

document.addEventListener(
  'DOMContentLoaded',
  () => {
    forEachPolyfill()
    splide()
  },
  false
)
